import models.*;

public class Prac6 {
  
  // Starts the app
  public static void main(String[] args) {
    Cuadrado sq1 = new Cuadrado("sq1", 4);
    Cuadrado sq2 = new Cuadrado("sq2", 4);
    Cuadrado sq3 = new Cuadrado("sq3", 6);
    System.out.println(sq1.equals(sq2));
    System.out.println(sq1.equals(sq3));
    System.out.println(sq1.toString());
    System.out.println(sq1.hashCode());
    System.out.println(sq2.hashCode());
    System.out.println(sq3.hashCode());
  }
}
