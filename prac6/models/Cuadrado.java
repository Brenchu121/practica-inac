package models;

public class Cuadrado extends Figura {

    // Constructors
    public Cuadrado() {
    }

    public Cuadrado(String nombre, float lado) {
      this.nombre = nombre;
      this.base = lado;
    }

    // Calc methods
    public float calcularPerimetro() {
      return this.base * (float)4;
    }

    public float calcularSuperficie() {
      return (this.base * this.base);
    }

    public boolean equals(Object compareTo) {
      return this.calcularPerimetro() == ((Cuadrado) compareTo).calcularPerimetro() 
        && this.calcularSuperficie() == ((Cuadrado) compareTo).calcularSuperficie();
    }
}

