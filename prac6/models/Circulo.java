package models;

public class Circulo extends Figura {

    // Constructors
    public Circulo() {
    }

    public Circulo(String nombre, float radio) {
      this.nombre = nombre;
      this.base = radio;
    }

    // Calc methods
    public float calcularPerimetro() {
      return this.base * (float)2 * (float)3.14;
    }

    public float calcularSuperficie() {
      return (this.base * this.base) * (float) 3.14;
    }

    public boolean equals(Object compareTo) {
      return this.calcularPerimetro() == ((Circulo) compareTo).calcularPerimetro() 
        && this.calcularSuperficie() == ((Circulo) compareTo).calcularSuperficie();
    }
}
