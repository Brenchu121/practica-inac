package models;

public class Figura {
    public String nombre;
    public float base;

    // Getter-Setters
    public String getNombre() {
      return this.nombre;
    }

    public void setNombre(String nombre) {
      this.nombre = nombre;
    }

    public float getBase() {
      return this.base;
    }

    public void setBase(float base) {
      this.base = base;
    }

    public Figura() {
    }

    public Figura(String nombre) {
      this.nombre = nombre;
    }

    public int hashCode() {
      int result = this.nombre.hashCode();
      result = 31 * result + String.valueOf(base).hashCode();
      return result;
    }

    public String toString() {
      return "{\"nombre\":\"" + this.nombre +
        "\",\"base\":" + this.base + "}";
    }
}
